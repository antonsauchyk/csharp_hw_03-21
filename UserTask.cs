﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_HW_03_21
{
    public class UserTask
    {
        public enum operationLabels
        {
            Equality = 1,
            Inequality = 2,
            Remainder = 3
        }

        public void Task1 ()
        {
            var userInput = Console.ReadLine();
            int userInputNumber = Int16.Parse(userInput);

            if (userInputNumber > 5 && userInputNumber < 10)
            {
                Console.WriteLine($"The number {userInputNumber} is greater than 5 and less than 10.");
            }
            else
            {
                Console.WriteLine($"The number {userInputNumber} is unknown.");
            }

            Console.WriteLine();
        }
        
        public void Task2 () {
            var userInput = Console.ReadLine();
            int userInputNumber = Int16.Parse(userInput);

            if (userInputNumber % 2 == 0)
            {
                Console.WriteLine($"The number {userInputNumber} is even.");
            }
            else
            {
                Console.WriteLine($"The number {userInputNumber} is uneven.");
            }

            Console.WriteLine();
        }

        public int Task3()
        {
            Console.WriteLine("Please enter an operation number:");
            Console.WriteLine($"1 - {operationLabels.Equality}.");
            Console.WriteLine($"2 - {operationLabels.Inequality}.");
            Console.WriteLine($"3 - {operationLabels.Remainder}.");

            var userInput = Console.ReadLine();
            int userInputNumber = Int16.Parse(userInput);

            switch (userInputNumber)
            {
                case 1:
                    Console.WriteLine($"You selected the {operationLabels.Equality}.");
                    Console.WriteLine();
                    return 1;
                case 2:
                    Console.WriteLine($"You selected the {operationLabels.Inequality}.");
                    Console.WriteLine();
                    return 2;
                case 3:
                    Console.WriteLine($"You selected the {operationLabels.Remainder}.");
                    Console.WriteLine();
                    return 3;
                default:
                    Console.WriteLine("The operation has not been found. Please check your input.");
                    Console.WriteLine();
                    return 0;
            }
        }
    
        public void Task4()
        {
            int userInputOperationNumber = Task3();

            Console.WriteLine("Please enter the first number:");
            var userInput1 = Console.ReadLine();

            Console.WriteLine("Please enter the second number:");
            var userInput2 = Console.ReadLine();

            int userInputNumber1 = Int16.Parse(userInput1);
            int userInputNumber2 = Int16.Parse(userInput2);

            switch (userInputOperationNumber)
            {
                case 1:
                    // Equality
                    bool inputsAreEqual = userInputNumber1 == userInputNumber2;
                    if (inputsAreEqual)
                    {
                        Console.WriteLine($"The numbers {userInputNumber1} and {userInputNumber2} are equal.");
                    }
                    else
                    {
                        Console.WriteLine($"The numbers {userInputNumber1} and {userInputNumber2} are not equal.");
                    }
                    
                    Console.WriteLine();
                    break;
                case 2:
                    // Inquality
                    bool inputsAreNotEqual = userInputNumber1 != userInputNumber2;
                    if (inputsAreNotEqual)
                    {
                        Console.WriteLine($"The numbers {userInputNumber1} and {userInputNumber2} are not equal.");
                    }
                    else
                    {
                        Console.WriteLine($"The numbers {userInputNumber1} and {userInputNumber2} are equal.");
                    }
                    Console.WriteLine();
                    break;
                case 3:
                    // Remainder
                    int remainder = userInputNumber1 % userInputNumber2;
                    Console.WriteLine($"The result of the operation equals to {remainder}.");
                    Console.WriteLine();
                    break;
                default:
                    Console.WriteLine("The operation has not been found. Please check your input.");
                    break;
            }


        }

        public void Task5()
        {
            Console.WriteLine("Please enter any number.");
            string userInputString = Console.ReadLine();
            // Initialize an array for storing digits
            int[] userInputDigits = new int[userInputString.Length];

            // Convert each char of a string to int and store it in an array
            for (int i = 0; i < userInputString.Length; i++)
            {
                userInputDigits[i] = Int32.Parse(userInputString[i].ToString());
            }

            // Calculate product of all digits in an array
            int digitsProduct = 1;
            foreach (int i in userInputDigits)
            {
                digitsProduct *= i;
            }

            // Calculate sum of all digits in an array
            int digitsSum = 0;
            foreach (int i in userInputDigits)
            {
                digitsSum += i;
            }

            Console.WriteLine($"The difference between the digits product {digitsProduct} and their sum {digitsSum} equals to {(digitsProduct-digitsSum).ToString()}");

        }
    }

}